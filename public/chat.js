// LECLERCQ Morgan et BESSANT Hugo TP2A

"use strict";
document.addEventListener("DOMContentLoaded", function(_e) {

    // socket ouverte vers le client
    var sock = io.connect();

    // Au démarrage : force l'affichage de l'écran de connexion
    document.getElementById("radio1").checked = true;
    document.getElementById("pseudo").focus();

    document.getElementById("login").appendChild(document.createTextNode(""));

    var pseudoConnect;
    var messageAEnvoyer;
    var listePersonnes;

    document.getElementById("btnConnecter").addEventListener("click", function() {
        connect();
    });

    document.getElementById("btnQuitter").addEventListener("click", function() {

        disconnect();
    });

    document.getElementById("btnEnvoyer").addEventListener("click", function() {
        if(document.getElementById("monMessage").value.substr(0,1)=='@'){
            var cpt=0;
            while(document.getElementById("monMessage").value.substr(cpt,1)!=' '){
                cpt++;
            }
            var dest=document.getElementById("monMessage").value.substr(1,cpt-1);
            
            envoyerMessagePrive(dest);
         
        }else{
            envoyerMessage();
        }
        
        
    });

    let connect=function(){
        pseudoConnect=document.getElementById("pseudo").value;
        sock.emit("login",pseudoConnect);
        sock.on("erreur-connexion",function(phrase){
            disconnect();
            alert(phrase);
            return;
        });
        document.getElementById("radio1").checked = false;
        document.getElementById("radio2").checked = true;
        afficheUtilisateurOnglet(pseudoConnect);
        
        
    }

    let disconnect=function(){
        clearUtilisateurOnglet();
        sock.emit("logout");
        document.getElementById("radio2").checked = false;
        document.getElementById("radio1").checked = true;
    }

    let afficheUtilisateurOnglet=function(pseudo){
        document.getElementById("login").childNodes[0].nodeValue=pseudo;   
    }

    let afficherListeUtilisateur=function(liste){
        for(let i=0;i<liste.length;i++){
            var textListePseudo=document.createTextNode(liste[i]);
            var p=document.createElement('p');
            p.appendChild(textListePseudo);
            document.getElementById("content").getElementsByTagName("aside")[0].appendChild(p);
        }
    }

    let envoyerMessage=function(){
        messageAEnvoyer=""+DateFormatCorrect(Date.now())+" - "+pseudoConnect+ " : "+document.getElementById("monMessage").value;
        var tab={from: pseudoConnect, to: null, text: messageAEnvoyer, date: Date.now()}
        sock.emit("message",tab);
    }

    let envoyerMessagePrive=function(dest){
        
        messageAEnvoyer=""+DateFormatCorrect(Date.now())+" - "+pseudoConnect+ " [privé] : "+document.getElementById("monMessage").value.substr(dest.length+1);
        console.log(messageAEnvoyer);
        var tab={from: pseudoConnect, to: dest, text: messageAEnvoyer, date: Date.now()};
        sock.emit("message",tab);
        
    }

    let afficherMessage=function(mess,classe){
        var textMessage=document.createTextNode(mess);
        var p1=document.createElement('p');
        p1.appendChild(textMessage);
        p1.className=classe;
        document.getElementById("content").getElementsByTagName("main")[0].appendChild(p1);
    }


    let DateFormatCorrect=function(date){
        //Ici format voulu est: <<HH:MM:SS>>
        var heures=Math.trunc((date/3600000)%24)+1;
        var minutes=Math.trunc((date/60000)%60);
        var secondes=Math.trunc((date/1000)%60);
        var dateBonFormat="";
        if(heures<10){
            dateBonFormat+="0"+heures;
        }else{
            dateBonFormat+=heures;
        }
        if(minutes<10){
            dateBonFormat+=":0"+minutes;
        }else{
            dateBonFormat+=":"+minutes;
        }
        if(secondes<10){
            dateBonFormat+=":0"+secondes;
        }else{
            dateBonFormat+=":"+secondes;
        }
        return dateBonFormat;
    }


    let afficherMessagePrive=function(mess,classe){
        
        if(pseudoConnect==mess.to){
            console.log(mess);
            var textMessage=document.createTextNode(mess.text);
            var p1=document.createElement('p');
            p1.appendChild(textMessage);
            p1.className=classe;
            document.getElementById("content").getElementsByTagName("main")[0].appendChild(p1); 
        }else{
            var id_avant=mess.text.indexOf("privé")+5;
            var id_apres=mess.text.indexOf("]");
            mess.text=mess.text.substr(0,id_avant)+" @"+mess.to+mess.text.substr(id_apres);
            var textMessage=document.createTextNode(mess.text);
            var p1=document.createElement('p');
            p1.appendChild(textMessage);
            p1.className=classe;
            document.getElementById("content").getElementsByTagName("main")[0].appendChild(p1); 
        }
        

    }

    let clearUtilisateursConnecte=function(){
        var p=document.getElementsByTagName("aside")[0].getElementsByTagName("p");
        var taille=p.length;
        for(let a=0;a<taille;a++){
           document.getElementsByTagName("aside")[0].removeChild(p[0]); 
        }
        
    }

    let updateListe=function(liste){
        clearUtilisateursConnecte();
        afficherListeUtilisateur(liste);

    }

    let clearUtilisateurOnglet=function(){
        document.getElementById("login").childNodes[0].nodeValue="";
    }

    

    sock.on("bienvenue",function(liste){
        updateListe(liste);
        listePersonnes=liste;
    });

    

    sock.on("liste",function(liste){
        updateListe(liste);
    });

    sock.on("message",function(mess){
        var classe;
        
        if(mess.from==null && mess.to==null){
            classe="system";
            afficherMessage(mess.text,classe);
        }
        if(mess.from==pseudoConnect && mess.to==null){
            classe="moi";
            afficherMessage(mess.text,classe);
        }
        if(mess.from!=pseudoConnect && mess.to==null && mess.from!=null){
            classe="";
            afficherMessage(mess.text,classe);
        }

        if(mess.from!=null && mess.to!=null){
            classe="mp";
            afficherMessagePrive(mess,classe);
        }
        
    });


    
    
});
    